﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox.Libraries.HaackFormatting {
  public interface ITextExpression {
    string Eval(object o);
  }
}
