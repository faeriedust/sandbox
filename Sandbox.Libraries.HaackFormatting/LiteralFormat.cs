﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox.Libraries.HaackFormatting {
  public class LiteralFormat : ITextExpression {
    public LiteralFormat(string literalText) {
      this.LiteralText = literalText;
    }
    public string LiteralText { get; private set; }
    public string Eval(object o) {
      string literalText = this.LiteralText
          .Replace("{{", "{")
          .Replace("}}", "}");
      return literalText;
    }
  }
}
