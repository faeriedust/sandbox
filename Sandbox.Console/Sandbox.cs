﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox.Main {
  public class Sandbox {
    public static void Main(string[] args) {
      //var anyOver5 = Enumerable.Range(1, 1000)
      //  .Select(x => { Console.WriteLine(x); return x; })
      //  .Any(x => x > 5);
      //Console.WriteLine(anyOver5);
      //String myString = String.Format("{LOL}", new { LOL = "hi" });
      //Console.WriteLine(myString);
      Console.Read();
    }
  }

  public static class Generators {
    public static IEnumerable<T> Where<T>(this IEnumerable<T> source, Func<T, bool> condition) {
      foreach (var x in source) {
        if (condition(x)) {
          yield return x;
        }
      }
    }

    public static IEnumerable<int> NumbersDivisibleBy5() {
      int x = 5;
      while (true) {
        yield return x;
        x += 5;
      }
    }
  }

  public static class StringExtensions {
    public static string AppendX(this string source) {
      return source + "x";
    }
  }
}
