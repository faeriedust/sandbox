﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox.WPF.ViewModels {
  public class MainWindowViewModel {
    public MainWindowViewModel() {
      this.ScrollViewerTabA = new ScrollViewerTabAViewModel();
      this.ScrollViewerTabB = new ScrollViewerTabBViewModel();
    }
    public ScrollViewerTabAViewModel ScrollViewerTabA { get; private set; }
    public ScrollViewerTabBViewModel ScrollViewerTabB { get; private set; }
  }
}
