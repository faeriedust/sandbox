﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sandbox.Libraries.Common;
using Sandbox.Models.Business;

namespace Sandbox.WPF.ViewModels {
  public class BaseScrollViewerTabViewModel {
    public BaseScrollViewerTabViewModel() {
      this.Customers = new List<CustomerModel>() {
        new CustomerModel("Fred"),
        new CustomerModel("George"),
        new CustomerModel("Sam"),
        new CustomerModel("Roger"),
        new CustomerModel("Frank"),
        new CustomerModel("Herbert"),
        new CustomerModel("Eric"),
        new CustomerModel("Sean")
      }.Select(c => new CustomerViewModel(c))
      .ToObservableCollection();
    }
    public ObservableCollection<CustomerViewModel> Customers { get; private set; }
  }
  public class ScrollViewerTabAViewModel : BaseScrollViewerTabViewModel {
    public ScrollViewerTabAViewModel() { }
  }
  public class ScrollViewerTabBViewModel : BaseScrollViewerTabViewModel {
    public ScrollViewerTabBViewModel() { }
  }
}
