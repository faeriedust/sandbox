﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sandbox.Models.Business;

namespace Sandbox.WPF.ViewModels {
  public class CustomerViewModel {
    public CustomerViewModel(CustomerModel customer) {
      _model = customer;
    }
    private readonly CustomerModel _model;
    public string Name {
      get { return _model.Name; }
      set { _model.Name = value; }
    }
  }
}
