﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sandbox.Core.TopologicalSorting;

namespace Sandbox.UnitTests.Helpers {
  public class TopologicallySortableNode : ITopologicallySortable<TopologicallySortableNode> {
    public TopologicallySortableNode() {
      this.Name = String.Empty;
      this.Principals = new List<TopologicallySortableNode>();
      this.DependencyString = String.Empty;
    }
    public string Name { get; set; }
    public string DependencyString { get; set; }
    public List<TopologicallySortableNode> Principals { get; set; }
    IEnumerable<TopologicallySortableNode> ITopologicallySortable<TopologicallySortableNode>.Principals {
      get { return this.Principals; }
    }
    public override string ToString() {
      return this.Name;
    }
  }
}
