﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox.UnitTests.Helpers {
  public static class TopologicalSorterTestHelpers {
    public static Dictionary<string, TopologicallySortableNode> CreateNodesAndDependencies(params Tuple<string, string>[] namesAndPrinicipalStrings) {
      var nodeDictionary = namesAndPrinicipalStrings
        .ToDictionary(x => x.Item1, x => new TopologicallySortableNode() { Name = x.Item1, DependencyString = x.Item2 });
      foreach (var node in nodeDictionary.Values) {
        node.Principals = nodeDictionary.Values.Where(n => node.DependencyString.Contains(n.Name)).ToList();
      }
      return nodeDictionary;
    }
  }
}
