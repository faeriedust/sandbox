﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sandbox.Core.Generics;

namespace Sandbox.UnitTests.Tests {
  [TestClass]
  public class GenericsTests {
    [TestMethod]
    public void BasicGenericsTestA() {
      int val = 1;
      val = val.Step(n => n + 1);
      Assert.AreEqual(2, val);
    }
  }
}
