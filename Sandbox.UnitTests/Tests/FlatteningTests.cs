﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sandbox.Core.Flattenting;
using Sandbox.Libraries.UnitTestFramework;

namespace Sandbox.UnitTests.Tests {

  [TestClass]
  public class FlatteningTests {
    [TestMethod]
    public void BasicFlatteningTest() {
      IEnumerable<string> lowNumbers = new string[] { "1", "2", "3" };
      IEnumerable<string> midNumbers = new string[] { "4", "5", "6" };
      IEnumerable<string> highNumbers = new string[] { "7", "8", "9" };
      IEnumerable<IEnumerable<string>> numbers = new IEnumerable<string>[] {
        lowNumbers,
        midNumbers,
        highNumbers
      };
      IEnumerable<string> lowLetters = new string[] { "a", "b", "c" };
      IEnumerable<string> midLetters = new string[] { "m", "n", "o" };
      IEnumerable<string> highLetters = new string[] { "x", "y", "z" };
      IEnumerable<IEnumerable<string>> letters = new IEnumerable<string>[] {
        lowLetters,
        midLetters,
        highLetters
      };
      IEnumerable<IEnumerable<IEnumerable<string>>> strings = new IEnumerable<IEnumerable<string>>[] {
        numbers,
        letters
      };
      IEnumerable<string> expectedNumbers = Enumerable.Range(1, 9).Select(n => n.ToString());
      IEnumerable<string> expectedLetters = new string[] { "a", "b", "c", "m", "n", "o", "x", "y", "z" };
      IEnumerable<string> expectedStrings = expectedNumbers.Concat(expectedLetters);
      CustomAssert.AreSequenceEqual(expectedNumbers, numbers.Flatten());
      CustomAssert.AreSequenceEqual(expectedLetters, letters.Flatten());
      //CustomAssert.AreSequenceEqual(expectedStrings, strings.Flatten());
    }
  }
}
