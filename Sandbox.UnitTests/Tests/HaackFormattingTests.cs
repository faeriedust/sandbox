﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sandbox.Libraries.HaackFormatting;

namespace Sandbox.UnitTests.Tests {
  [TestClass]
  public class HaackFormattingTests {
    [TestMethod]
    public void BasicHaackFormattingTestA() {
      var expr = new FormatExpression("{lol}");
      Assert.AreEqual("hi", expr.Eval(new { lol = "hi" }));
    }
  }
}
