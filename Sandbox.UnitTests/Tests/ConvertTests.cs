﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sandbox.Core.Animals;

namespace Sandbox.UnitTests.Tests {
  [TestClass]
  public class MyTestClass {
    [TestMethod]
    public void ConvertIntToInt() {
      int x = 6;
      int y = Convert.ToInt32(x);
      Assert.AreEqual(x, y);
    }
  }
}
