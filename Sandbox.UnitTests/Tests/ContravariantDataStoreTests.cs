﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sandbox.Core.Animals;
using Sandbox.Core.ContravariantDataStorage;

namespace Sandbox.UnitTests.Tests {
  [TestClass]
  public class ContravariantDataStoreTests {
    [TestMethod]
    public void BasicContravariantDataStoreTest1() {
      IContravariantDictionary<int, Animal> giraffes = new ContravariantDictionary<int, Giraffe>();
      IContravariantDictionary<int, Animal> lions = new ContravariantDictionary<int, Lion>();

      List<IContravariantDictionary<int, Animal>> animalDictionaries = new List<IContravariantDictionary<int, Animal>>();
      animalDictionaries.Add(giraffes);
      animalDictionaries.Add(lions);
      Assert.AreEqual(2, animalDictionaries.Count);
    }
    [TestMethod]
    public void TryGetValueTest() {
      ContravariantDictionary<int, Giraffe> giraffes = new ContravariantDictionary<int, Giraffe>();
      giraffes[1] = new Giraffe();
      giraffes[2] = new Giraffe();

      IContravariantDictionary<int, Animal> contravariantGiraffes = giraffes;
      Animal myGiraffe = null;
      bool containsMyGiraffe;

      containsMyGiraffe = contravariantGiraffes.TryGetValue(3, out myGiraffe);
      Assert.IsFalse(containsMyGiraffe);
      Assert.IsTrue(myGiraffe == null);

      containsMyGiraffe = contravariantGiraffes.TryGetValue(2, out myGiraffe);
      Assert.IsTrue(containsMyGiraffe);
      Assert.IsTrue(myGiraffe != null);
    }
  }
}
