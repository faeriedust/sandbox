﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sandbox.UnitTests.Helpers;
using Sandbox.Core.TopologicalSorting;
using Sandbox.Libraries.Common;
using Sandbox.Libraries.UnitTestFramework;

namespace Sandbox.UnitTests.Tests {
  [TestClass]
  public class TopologicalSortTests {
    [TestMethod]
    public void MultipleCharacteristicPrincipalsTest() {
      var characteristicDictionary = TopologicalSorterTestHelpers.CreateNodesAndDependencies(
        Tuple.Create("A", "BC"),
        Tuple.Create("B", ""),
        Tuple.Create("C", ""));
      var characteristicA = characteristicDictionary["A"];
      var expectedPrincipals = characteristicDictionary.GetValues("B", "C");
      CustomAssert.AreSetEqual(expectedPrincipals, characteristicA.Principals);
    }
    [TestMethod]
    public void DoNotDependOnSelf() {
      var characteristicDictionary = TopologicalSorterTestHelpers.CreateNodesAndDependencies(
        Tuple.Create("A", "A"));
      var expectedCycleValues = characteristicDictionary.GetValues("A");
      var actualCycle = characteristicDictionary.Values.FindCycle();
      CustomAssert.AreSetEqual(expectedCycleValues, actualCycle);
    }
    /// <summary>
    /// Tests that FindCycle correctly reports no cycles in a known non-cycling set of Characteristics
    /// </summary>
    [TestMethod]
    public void DetectNoDirectCyclesTest() {
      // create Tuple for each characteristic to be tested by cycledetection function
      // this set is valid
      var characteristicDictionary = TopologicalSorterTestHelpers.CreateNodesAndDependencies(
        Tuple.Create("A", "B"),
        Tuple.Create("B", "")
      );
      Assert.IsFalse(characteristicDictionary.Values.FindCycle().Any());
    }
    /// <summary>
    /// Tests that FindCycle correctly detects direct cycles in a known direct-cycling set of Characteristics
    /// </summary>
    [TestMethod]
    public void DetectDirectCycleTest() {
      // this set is not valid
      var characteristicDictionary = TopologicalSorterTestHelpers.CreateNodesAndDependencies(
        Tuple.Create("A", "B"),
        Tuple.Create("B", "A")
      );
      var expectedCycleValues = characteristicDictionary.GetValues("A", "B");
      var actualCycle = characteristicDictionary.Values.FindCycle();
      CustomAssert.AreSetEqual(expectedCycleValues, actualCycle);
    }
    /// <summary>
    /// Tests that FindCycle correctly detects indirect cycles in a known indirect-cycling set of Characteristics
    /// </summary>
    [TestMethod]
    public void DetectIndirectCyclesTest() {
      var characteristicDictionary = TopologicalSorterTestHelpers.CreateNodesAndDependencies(
        Tuple.Create("A", "C"),
        Tuple.Create("B", "A"),
        Tuple.Create("C", "B")
      );
      var expectedCycleValues = characteristicDictionary.GetValues("A", "B", "C");
      var actualCycle = characteristicDictionary.Values.FindCycle();
      CustomAssert.AreSetEqual(expectedCycleValues, actualCycle);
    }

    [TestClass]
    // E's sample for me
    public class haracteristicTests {
      [TestMethod]
      public void BasicToposortTest() {
        // make directed graph that has no cycles
        var characteristicDictionary = TopologicalSorterTestHelpers.CreateNodesAndDependencies(
          Tuple.Create("A", "B"),
          Tuple.Create("B", "C"),
          Tuple.Create("C", ""));
        var expectedSortedCharacteristics = characteristicDictionary.GetValues("C", "B", "A");
        var actualSortedCharacteristics = characteristicDictionary.Values.TopologicallySort();
        CustomAssert.AreSequenceEqual(expectedSortedCharacteristics, actualSortedCharacteristics);
      }


      /// <summary>
      /// Ensure topological sort correctly sorts graph of characteristics that has no cycles
      /// </summary>
      [TestMethod]
      public void ToposortValidCharacteristicsGraph() {
        // make directed graph that has no cycles
        var characteristicDictionary = TopologicalSorterTestHelpers.CreateNodesAndDependencies(
          Tuple.Create("E", "DF"),
          Tuple.Create("D", "BC"),
          Tuple.Create("B", "A"),
          Tuple.Create("F", "CD"),
          Tuple.Create("C", "AB"),
          Tuple.Create("A", ""));
        var expectedSortedCharacteristics = characteristicDictionary.GetValues("A", "B", "C", "D", "F", "E");
        var actualSortedCharacteristics = characteristicDictionary.Values.TopologicallySort();
        CustomAssert.AreSequenceEqual(expectedSortedCharacteristics, actualSortedCharacteristics);
      }
      [TestMethod]
      public void ToposortFailCharacteristicsGraphCycle() {
        // make graph with cycle (this is indirect cycle)
        var characteristicDictionary = TopologicalSorterTestHelpers.CreateNodesAndDependencies(
          Tuple.Create("A", "C"),
          Tuple.Create("B", "A"),
          Tuple.Create("C", "B")
        );
        List<TopologicallySortableNode> sortedList;
        Assert.IsFalse(characteristicDictionary.Values.TryTopologicallySort(out sortedList));
      }
    }
  }
}
