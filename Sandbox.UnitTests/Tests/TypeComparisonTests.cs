﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sandbox.Core.Animals;
using Sandbox.Core.TypeComparisons;

namespace Sandbox.UnitTests.Tests {
  [TestClass]
  public class TypeComparisonTests {
    [TestMethod]
    public void LionHasBaseTypeAnimal() {
      Console.WriteLine();
      Assert.IsTrue(typeof(Lion).HasBaseType(typeof(Animal)));
    }
    [TestMethod]
    public void AnimalDoesNotHaveBaseTypeLion() {
      Assert.IsFalse(typeof(Animal).HasBaseType(typeof(Lion)));
    }
    [TestMethod]
    public void AnimalIsAssignableFromLion() {
      Assert.IsTrue(typeof(Animal).IsAssignableFrom(typeof(Lion)));
    }
    [TestMethod]
    public void LionIsNotAssignableFromAnimal() {
      Assert.IsFalse(typeof(Lion).IsAssignableFrom(typeof(Animal)));
    }
  }
}
