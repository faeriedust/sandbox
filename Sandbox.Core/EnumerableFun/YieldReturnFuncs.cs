﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox.Core.EnumerableFun {
  public static class EnumerableHack {
    public static IEnumerable<int> Range() {
      Func<int> initialStateCreate = () => 1;
      Func<Func<int>, Func<Func<int>>> initialFunctionCreate = x => {
        return () => {
          int n = x();
          return () => n++;
        };
      };
      Func<Func<int>> initialize = () => initialFunctionCreate(initialStateCreate)();
      return new MyEnumerable<int>(initialize);
    }
    private class MyEnumerable<T> : IEnumerable<T>, IEnumerator<T> {
      #region Constructorsre
      public MyEnumerable(Func<Func<T>> initialize) {
        this.Initialize = initialize;
        this.CurrentFunction = this.Initialize();
      }
      #endregion
      #region Properties
      private object InitialState { get; set; }
      private Func<Func<T>> Initialize { get; set; }
      private Func<T> CurrentFunction { get; set; }
      public T CurrentValue { get; set; }
      #endregion
      #region IEnumerable
      public IEnumerator<T> GetEnumerator() {
        return new MyEnumerable<T>(this.Initialize);
      }
      IEnumerator IEnumerable.GetEnumerator() {
        return this.GetEnumerator();
      }
      #endregion
      #region IEnumerator
      public T Current {
        get { return this.CurrentValue; }
      }
      public void Dispose() {
        this.Initialize = null;
        this.CurrentFunction = null;
        this.CurrentValue = default(T);
      }
      object IEnumerator.Current {
        get { return this.Current; }
      }
      public bool MoveNext() {
        this.CurrentValue = this.CurrentFunction();
        return true;
      }
      public void Reset() {
        this.CurrentFunction = this.Initialize();
      }
      #endregion
    }
  }
}
