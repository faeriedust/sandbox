﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox.Core.ContravariantDataStorage {
  public interface IContravariantDictionary<TKey, out TValue> {
    // Summary:
    //     Gets an enumerable collection that contains the keys in the read-only dictionary.
    //
    // Returns:
    //     An enumerable collection that contains the keys in the read-only dictionary.
    IEnumerable<TKey> Keys { get; }
    //
    // Summary:
    //     Gets an enumerable collection that contains the values in the read-only dictionary.
    //
    // Returns:
    //     An enumerable collection that contains the values in the read-only dictionary.
    IEnumerable<TValue> Values { get; }
    // Summary:
    //     Gets the element that has the specified key in the read-only dictionary.
    //
    // Parameters:
    //   key:
    //     The key to locate.
    //
    // Returns:
    //     The element that has the specified key in the read-only dictionary.
    //
    // Exceptions:
    //   System.ArgumentNullException:
    //     key is null.
    //
    //   System.Collections.Generic.KeyNotFoundException:
    //     The property is retrieved and key is not found.
    TValue this[TKey key] { get; }
    // Summary:
    //     Determines whether the read-only dictionary contains an element that has
    //     the specified key.
    //
    // Parameters:
    //   key:
    //     The key to locate.
    //
    // Returns:
    //     true if the read-only dictionary contains an element that has the specified
    //     key; otherwise, false.
    //
    // Exceptions:
    //   System.ArgumentNullException:
    //     key is null.
    bool ContainsKey(TKey key);
  }
  public class ContravariantDictionary<TKey, TValue> : Dictionary<TKey, TValue>, IContravariantDictionary<TKey, TValue> {
    public ContravariantDictionary() { }
    IEnumerable<TKey> IContravariantDictionary<TKey, TValue>.Keys {
      get { return this.Keys; }
    }
    IEnumerable<TValue> IContravariantDictionary<TKey, TValue>.Values {
      get { return this.Values; }
    }
    TValue IContravariantDictionary<TKey, TValue>.this[TKey key] {
      get { return this[key]; }
    }
    bool IContravariantDictionary<TKey, TValue>.ContainsKey(TKey key) {
      return this.ContainsKey(key);
    }
  }
  public static class ContravariantDictionaryExtensions {
    //
    // Summary:
    //     Gets the value that is associated with the specified key.
    //
    // Parameters:
    //   key:
    //     The key to locate.
    //
    //   value:
    //     When this method returns, the value associated with the specified key, if
    //     the key is found; otherwise, the default value for the type of the value
    //     parameter. This parameter is passed uninitialized.
    //
    // Returns:
    //     true if the object that implements the System.Collections.Generic.IReadOnlyDictionary<TKey,TValue>
    //     interface contains an element that has the specified key; otherwise, false.
    //
    // Exceptions:
    //   System.ArgumentNullException:
    //     key is null.
    public static bool TryGetValue<TKey, TValue>(this IContravariantDictionary<TKey, TValue> source, TKey key, out TValue value) {
      if (source.ContainsKey(key)) {
        value = source[key];
        return true;
      } else {
        value = default(TValue);
        return false;
      }
    }
  }
}
