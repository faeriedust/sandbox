﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Threading.Tasks;

namespace Sandbox.Core.Flattenting {
  public static class FlattenExtensions {
    public static IEnumerable<T> Flatten<T>(this IEnumerable<IEnumerable<T>> source) {
      return source.SelectMany(x => x).Flatten();
    }
    public static IEnumerable<T> Flatten<T>(this IEnumerable<T> source) {
      return source;
    }
  }
}