﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox.Core.FluentSyntax {
  internal class Bla<TA, TB> {
    public Bla() { }
    public Bla(TA a, TB b) {
      this.A = a;
      this.B = b;
    }
    public TA A { get; set; }
    public TB B { get; set; }
  }
  internal static class BlaMethods {
    public static Bla<TA, TB> CreateBla<TA, TB>(TA a, TB b) {
      return new Bla<TA, TB>(a, b);
    }
    public static Bla<TA, TB> CreateBlaEFFriendly<TA, TB>(TA a, TB b) {
      return new Bla<TA, TB>() {
        A = a,
        B = b
      };
    }
    public static BlaBuilder<TA> BuildBla<TA>(TA a) {
      return new BlaBuilder<TA>(a);
    }
  }
  internal class BlaBuilder<TA> {
    public BlaBuilder(TA a) { _A = a; }
    protected TA _A { get; set; }
    public Bla<TA, TB> WithB<TB>(TB b) {
      return new Bla<TA, TB>(_A, b);
    }
  }
  internal class BlaFun {
    public void Test() {
      var a = new { Hi = "hi" };
      var b = new { Lol = "lol" };

      var x = BlaMethods.CreateBla(a, b);
      var y = BlaMethods.BuildBla(a).WithB(b);
    }
  }
}
