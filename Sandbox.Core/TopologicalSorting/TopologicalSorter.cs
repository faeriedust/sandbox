﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sandbox.Libraries.Common;

namespace Sandbox.Core.TopologicalSorting {
  public static class TopologicalSorter {
    private enum NodeState : int {
      DEAD,
      ALIVE,
      PROCESSING
    }
    #region TopologicallySort
    /// <summary>
    /// Sorts a list of items with dependencies such that items without dependencies come first.
    /// </summary>
    /// <typeparam name="T">Type of items, which must implement <see cref="ITopologicSortable"/>.</typeparam>
    /// <param name="graph">Set of items to sort.</param>
    /// <returns>A sorted list of item, where items with dependencies after items without dependencies.</returns>
    /// <remarks>
    /// If you don't want to deal with the exception, use the TryTopologicallySort version
    /// </remarks>
    /// <exception cref="SMS.Libraries.Core.Interface.Extensions.TopologicalSortException">Thrown if there is a cycle.</exception>
    public static List<T> TopologicallySort<T>(this IEnumerable<T> graph)
      where T : ITopologicallySortable<T> {
      return graph.TopologicallySort(x => x.Principals);
    }
    /// <summary>
    /// Sorts a list of items with dependencies such that items without dependencies come first.
    /// </summary>
    /// <typeparam name="T">Type of items, which must implement <see cref="ITopologicSortable"/>.</typeparam>
    /// <param name="graph">Set of items to sort.</param>
    /// <returns>A sorted list of item, where items with dependencies after items without dependencies.</returns>
    /// <remarks>
    /// If you don't want to deal with the exception, use the TryTopologicallySort version
    /// </remarks>
    /// <exception cref="SMS.Libraries.Core.Interface.Extensions.TopologicalSortException">Thrown if there is a cycle.</exception>
    public static List<T> TopologicallySort<T>(this IEnumerable<T> graph, Func<T, IEnumerable<T>> getPrincipals) {
      List<T> sortedList;
      if (graph.TryTopologicallySort(out sortedList, getPrincipals)) {
        return sortedList;
      } else {
        throw new TopologicalSortException("Cycle detected!");
      }
    }
    #endregion
    #region FindCycle
    public static List<T> FindCycle<T>(this IEnumerable<T> graph)
      where T : ITopologicallySortable<T> {
      return graph.FindCycle(x => x.Principals);
    }
    public static List<T> FindCycle<T>(this IEnumerable<T> graph, Func<T, IEnumerable<T>> getPrincipals) {
      List<T> sortedList;
      List<T> cycle;
      graph.TryTopologicallySort(out sortedList, out cycle, getPrincipals);
      return cycle;
    }
    #endregion
    #region TryTopologicallySort
    public static bool TryTopologicallySort<T>(this IEnumerable<T> graph, out List<T> sortedList)
      where T : ITopologicallySortable<T> {
      return TopologicalSorter.TryTopologicallySort(graph, out sortedList, x => x.Principals);
    }
    public static bool TryTopologicallySort<T>(this IEnumerable<T> graph, out List<T> sortedList, Func<T, IEnumerable<T>> getPrincipals) {
      List<T> cycle;
      return TopologicalSorter.TryTopologicallySort(graph, out sortedList, out cycle, getPrincipals);
    }
    #endregion
    #region TryTopologicallySortWithCycle
    /// <summary>
    /// Attempts to sorts a list of items with dependencies such that items without dependencies come first.
    /// </summary>
    /// <returns>true if sort was successful, otherwise false (in case of a cycle)</returns>
    public static bool TryTopologicallySort<T>(this IEnumerable<T> graph, out List<T> sortedList, out List<T> cycle)
      where T : ITopologicallySortable<T> {
      return graph.TryTopologicallySort(out sortedList, out cycle, x => x.Principals);
    }
    /// <summary>
    /// Attempts to sorts a list of items with dependencies such that items without dependencies come first.
    /// </summary>
    /// <returns>true if sort was successful, otherwise false (in case of a cycle)</returns>
    public static bool TryTopologicallySort<T>(this IEnumerable<T> graph, out List<T> sortedList, out List<T> cycle, Func<T, IEnumerable<T>> getPrincipals) {
      List<T> graphList = graph.ToList();
      cycle = new List<T>();
      sortedList = new List<T>();
      Dictionary<T, NodeState> nodeStates = graph.ToDictionary(node => node, node => NodeState.ALIVE);
      foreach (var node in graphList) {
        IEnumerable<T> currentPath = node.YieldItem();
        IEnumerable<T> fullPath;
        if (!_TryVisitNode<T>(node, nodeStates, sortedList, currentPath, out fullPath, getPrincipals)) {
          cycle = fullPath.SkipWhile(n => !n.Equals(node)).ToList();
          sortedList = null;
          return false;
        }
      }
      return true;
    }
    #endregion
    #region Helpers
    private static bool _TryVisitNode<T>(T node, Dictionary<T, NodeState> nodeStates, List<T> sortedList, IEnumerable<T> oldPath, out IEnumerable<T> fullPath, Func<T, IEnumerable<T>> getPrincipals) {
      switch (nodeStates[node]) {
        case NodeState.DEAD:
          fullPath = oldPath;
          return true;
        case NodeState.PROCESSING:
          fullPath = oldPath;
          return false;
        default:
          nodeStates[node] = NodeState.PROCESSING;
          foreach (var principal in getPrincipals(node)) {
            IEnumerable<T> currentPath = oldPath.Concat(principal.YieldItem());
            if (!_TryVisitNode(principal, nodeStates, sortedList, currentPath, out fullPath, getPrincipals)) { return false; }
          }
          nodeStates[node] = NodeState.DEAD;
          sortedList.Add(node);
          fullPath = oldPath.Concat(node.YieldItem());
          return true;
      }
    }
    #endregion
  }
  public interface ITopologicallySortable<T> {
    IEnumerable<T> Principals { get; }
  }
  public class TopologicalSortException : Exception {
    public TopologicalSortException() : base() { }
    public TopologicalSortException(string message) : base(message) { }
    public TopologicalSortException(string message, Exception innerException) : base(message, innerException) { }
  }
}
