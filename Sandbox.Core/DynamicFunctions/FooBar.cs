﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sandbox.Core.Animals;

namespace Sandbox.Core.DynamicFunctions {
  public class FooBar {
    public FooBar() { }
    public IFoo MyFoo { get { return new Foo<Lion>(); } }
    public Func<dynamic, Giraffe> MyFooBar { get { return x => this.MyFoo.GetBar(x); } }
  }
}
