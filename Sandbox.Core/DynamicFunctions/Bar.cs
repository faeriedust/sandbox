﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox.Core.DynamicFunctions {
  public interface IBar {
    object Value { get; }
  }
  public interface IBar<out T> : IBar {
    new T Value { get; }
  }
  public class Bar<T> : IBar<T> {
    public Bar() { }
    public T Value { get; set; }
    object IBar.Value { get { return this.Value; } }
  }
}
