﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sandbox.Core.Animals;

namespace Sandbox.Core.DynamicFunctions {
  public interface IFoo {
    Func<Lion, IBar> GetBar { get; }
  }
  public interface IFoo<out T> : IFoo {
    new Func<Lion, IBar<T>> GetBar { get; }
  }
  public class Foo<T> : IFoo<T> {
    public Foo() { this.GetBar = x => new Bar<T>(); }
    public Func<Lion, Bar<T>> GetBar { get; set; }
    Func<Lion, IBar<T>> IFoo<T>.GetBar {
      get { return this.GetBar; }
    }
    Func<Lion, IBar> IFoo.GetBar {
      get { return this.GetBar; }
    }
  }
}
