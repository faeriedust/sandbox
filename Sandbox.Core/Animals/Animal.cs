﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox.Core.Animals {
  public class Animal {
    public Animal() { }
  }
  public class Giraffe : Animal {
    public Giraffe() { }
  }
  public class Lion : Animal {
    public Lion() { }
  }
}
