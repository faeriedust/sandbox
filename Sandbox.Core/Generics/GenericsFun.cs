﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox.Core.Generics {
  public static class GenericsFun {
    public static T Step<T>(this T source, Func<T, T> stepFunc) {
      return stepFunc(source);
    }
  }
}
