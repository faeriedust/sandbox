﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox.Core.TypeComparisons {
  public static class TypeExtensions {
    public static bool HasBaseType(this Type type, Type baseType) {
      return baseType.IsAssignableFrom(type);
    }
  }
}
