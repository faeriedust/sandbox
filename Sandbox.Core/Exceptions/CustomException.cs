﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox.Core.Exceptions {
  public class CustomException : Exception {
    public CustomException() : base() {
      this.strings = new List<String>();
    }
    public CustomException(string messageFormatString, params object[] messageFormatParams)
      : this(null, messageFormatString, messageFormatParams) { }
    public CustomException(Exception innerException, string messageFormatString, params object[] messageFormatParams)
      : base(String.Format(messageFormatString, messageFormatParams), innerException) { }
    public List<String> strings { get; set; }
  }     
}
