﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sandbox.Libraries.Common;

namespace Sandbox.Libraries.UnitTestFramework {
  public static class CustomAssert {
    public static void AreSequenceEqual<T>(IEnumerable<T> expected, IEnumerable<T> actual) {
      Assert.IsTrue(expected.SequenceEqual(actual),
        String.Format("Sequence not equal.\nExpected:{0}\nActual:{1}",
        expected.Delimit(x => x.ToString()),
        actual.Delimit(x => x.ToString())));
    }
    public static void AreSetEqual<T>(IEnumerable<T> expected, IEnumerable<T> actual) {
      Assert.IsTrue(expected.SetEqual(actual),
        String.Format("Set not equal.\nExpected:{0}\nActual:{1}",
        expected.Delimit(x => x.ToString()),
        actual.Delimit(x => x.ToString())));
    }
  }
}
