﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox.Models.Business {
  public class CustomerModel {
    public CustomerModel(string name) {
      this.Name = name;
      this.Orders = new List<OrderModel>();
    }
    public string Name { get; set; }
    public ICollection<OrderModel> Orders { get; set; }
  }
}
