﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox.Models.Business {
  public class OrderModel {
    public OrderModel() {
      this.OrderNumber = Guid.NewGuid();
    }
    public Guid OrderNumber { get; set; }
  }
}
