﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;

namespace Sandbox.Libraries.Common {
  public static class StringExtensions {
    /// <summary>
    /// Merges a collection of strings into a single string, separating each string with a delimiter.
    /// Automatically removes null and empty strings.
    /// </summary>
    /// <param name="strings">The collection of strings to merge.</param>
    /// <param name="delimiter">The string to insert between each element in <paramref name="strings" />.  Defaults to ", " (a comma and a following space).</param>
    /// <param name="trim">If true, trims whitespace on both sides of the string before delimiting.</param>
    /// <param name="removeEmpty">If true, removes empty strings (after optional trimming (see <paramref name="trim"/>).</param>
    /// <returns>A comma-delimited string, composed of the strings in the collection.</returns>
    public static string Delimit(this IEnumerable<string> strings, string delimiter = ", ", bool trim = true, bool removeEmpty = true) {
      Contract.Requires(strings != null);
      Contract.Ensures(Contract.Result<string>() != null);
      return !strings.Any() ? "" : strings.Select(s => trim ? s.Trim() : s)
        .Where(s => !removeEmpty || !string.IsNullOrWhiteSpace(s))
        .Aggregate((cur, s) => cur + (cur == "" ? "" : delimiter) + s);
    }
    public static string Delimit<T>(this IEnumerable<T> source, Func<T, string> convert) {
      return source.Select(convert).Delimit();
    }
  }
}
