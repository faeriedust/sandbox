﻿using System.Collections.Generic;

namespace Sandbox.Libraries.Common {
  public static class ObjectExtensions {
    public static IEnumerable<T> YieldItem<T>(this T source) {
      yield return source;
    }
  }
}
