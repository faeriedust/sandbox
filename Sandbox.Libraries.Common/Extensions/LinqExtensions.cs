﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox.Libraries.Common {
  public static class LinqExtensions {
    public static IEnumerable<TValue> GetValues<TKey, TValue>(this IDictionary<TKey, TValue> source, params TKey[] keys) {
      return keys.Select(key => source[key]);
    }
    public static bool ContainsAll<T>(this IEnumerable<T> source, IEnumerable<T> second) {
      return second.All(x => source.Contains(x));
    }
    public static bool SetEqual<T>(this IEnumerable<T> source, IEnumerable<T> second) {
      var x = source.ToList();
      var y = second.ToList();
      return x.ContainsAll(y) && y.ContainsAll(x);
    }
    public static ObservableCollection<T> ToObservableCollection<T>(this IEnumerable<T> source) {
      var collection = new ObservableCollection<T>();
      foreach (var item in source) {
        collection.Add(item);
      }
      return collection;
    }
  }
}
