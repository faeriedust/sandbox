﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Sandbox.Libraries.WPF.Controls {
  /// <summary>
  /// List View that allows user to scroll last item to top
  /// </summary>
  public class ScrollLastItemToTopListView : ListView, INotifyPropertyChanged {
    public ScrollLastItemToTopListView() : base() {
      this.SizeChanged += (sender, e) => { this.OnPropertyChanged("ItemsPanelMargin"); };
    }
    #region PropertyChanged
    public event PropertyChangedEventHandler PropertyChanged;
    public void OnPropertyChanged(string propertyName) {
      if (this.PropertyChanged != null) {
        var args = new PropertyChangedEventArgs(propertyName);
        this.PropertyChanged(this, args);
      }
    }
    #endregion
    private Double _lastItemHeight {
      get {
        if (this.ItemContainerGenerator.Status == System.Windows.Controls.Primitives.GeneratorStatus.ContainersGenerated) {
          object lastItem = null;
          foreach (object currentItem in this.Items) {
            lastItem = currentItem;
          }
          if (lastItem != null) {
            ListViewItem container = (ListViewItem) this.ItemContainerGenerator.ContainerFromItem(lastItem);
            Border itemBorder = (Border) VisualTreeHelper.GetChild(container, 0);
            ContentPresenter presenter = (ContentPresenter) VisualTreeHelper.GetChild(itemBorder, 0);
            return presenter.ActualHeight;
          } else {
            return 0;
          }
        } else {
          return 0;
        }
      }
    }
    public Thickness ItemsPanelMargin {
      get {
        return new Thickness(0, 0, 0, this.ActualHeight - _lastItemHeight);
      }
    }
  }
}
